﻿using System;
using PFPAutomation.Models;

namespace PFPAutomation.Factories
{
    public class ClientFactory
    {
        public Client Create(string email)
        {
            if (email == null)
                throw new ArgumentNullException(nameof(email));

            return new Client
            {
                FirstName = "Bobo",
                LastName = Convert.ToBase64String(Guid.NewGuid().ToByteArray()),
                Type = ClientTypeValue.Person,
                Dob = new DateTime(1980, 5, 14),
                Gender = GenderTypeValue.Male,
                CustomerRef = 3746587274,
                NiNumber = "ab111222b",
                Email = email,
                PfpPassword = "TestPfp" + DateTime.Now.Ticks,
                AddressLine1 = "Line 1",
                AddressLine2 = "Line 2",
                AddressLine3 = "Line 3",
                AddressLine4 = "Line 4",
                Town = "Small town",
                County = "Angus",
                Postcode = "CF10 3MN",
                Country = "United Kingdom",
                ResidentFrom = new DateTime(1990, 5, 14),
                Telephone = "445454535"
            };
        }
    }
}