﻿namespace PFPAutomation.Models
{
    public enum GenderTypeValue
    {
        Female = 0,
        Male = 1,
        Unknown = 2
    }
}