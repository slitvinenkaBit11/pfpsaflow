﻿using System;

namespace PFPAutomation.Models
{
    public class Client
    {
        public ClientTypeValue Type { get; set; }
        public DateTime Dob { get; set; }
        public GenderTypeValue Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long CustomerRef { get; set; }
        public string NiNumber { get; set; }
        public string Email { get; set; }
        public string PfpPassword { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public DateTime ResidentFrom { get; set; }
        public string Telephone { get; set; }
    }
}