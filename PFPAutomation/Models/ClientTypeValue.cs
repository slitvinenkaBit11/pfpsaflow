﻿namespace PFPAutomation.Models
{
    public enum ClientTypeValue
    {
        Person = 0,
        Trust = 1,
        Corporate = 2,
    }
}