﻿using System;
using OpenQA.Selenium.Chrome;

namespace PFPAutomation.Services
{
    public class CompositeMailService
    {
        private readonly IMailService[] services;
        public IMailService CurrentService { get; private set; }

        public CompositeMailService(IMailService[] services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            this.services = services;
        }

        public string GetMailAddress(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));

            foreach (var service in services)
            {
                var mailAddress = service.CreateMailBox(mailboxPrefix, driver);

                if (string.IsNullOrEmpty(mailAddress)) continue;

                CurrentService = service;

                return mailAddress;
            }

            throw new ApplicationException("All mail providers are not working");
        }

        public string GetRegistrationLink(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));

            var link = CurrentService.GetRegistrationLink(mailboxPrefix, driver);

            return link;
        }
    }
}