﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using PFPAutomation.Helpers;

namespace PFPAutomation.Services
{
    public class ThrowAwayMailService : IMailService
    {
        public string CreateMailBox(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            var available = NavigateToInbox(mailboxPrefix, driver);
            var mailbox = "";

            if (!available) return mailbox;

            var mailAddress = By.Id("email");

            mailbox = driver.FindElement(mailAddress).Text;

            return mailbox;
        }

        public string GetRegistrationLink(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            NavigateToInbox(mailboxPrefix, driver);

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(20),
                PollingInterval = TimeSpan.FromMilliseconds(300)
            };

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            var pfpEmailLabel = By.XPath("//a[contains(text(),'Personal Finance Portal - Registration')]");
            var elementExists = driver.FindElements(pfpEmailLabel).Any();

            if (!elementExists)
            {
                for (var i = 0; i < 20; i++)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(15));
                    driver.Navigate().Refresh();
                    elementExists = driver.FindElements(pfpEmailLabel).Any();

                    if (elementExists) break;
                }
            }

            wait.Until(ExpectedConditions.ElementIsVisible(pfpEmailLabel));
            driver.FindElement(pfpEmailLabel).Click();

            var activateRegistrationLink = By.XPath($"//a[contains(@href,'{DriverExtensions.GetRedirectPath()}')]");
            var redirectUrl = driver.FindElement(activateRegistrationLink).GetAttribute("href");

            return redirectUrl;
        }

        private bool NavigateToInbox(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            driver.Navigate().GoToUrl("http://www.throwawaymail.com/");


            var mailAddress = By.Id("email");
            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(20),
                PollingInterval = TimeSpan.FromMilliseconds(300)
            };

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            try
            {
                wait.Until(ExpectedConditions.ElementIsVisible(mailAddress));

                return true;
            }
            catch (TimeoutException)
            {
                return false;
            } 
        }
    }
}