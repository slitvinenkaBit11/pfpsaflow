﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using PFPAutomation.Helpers;

namespace PFPAutomation.Services
{
    public class MaildropService : IMailService
    {
        public string CreateMailBox(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            var available = NavigateToInbox(mailboxPrefix, driver);
            var mailbox = "";

            if (!available) return mailbox;

            var mailAddress = By.Id("mainaddr");

            mailbox = driver.FindElement(mailAddress).Text;

            return mailbox;
        }

        public string GetRegistrationLink(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            NavigateToInbox(mailboxPrefix, driver);

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(20),
                PollingInterval = TimeSpan.FromMilliseconds(300)
            };

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            var pfpEmailLabel = By.XPath("//a[contains(text(),'Personal Finance Portal - Registration')]");
            var elementExists = driver.FindElements(pfpEmailLabel).Any();

            if (!elementExists)
            {
                for (var i = 0; i < 20; i++)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    driver.Navigate().Refresh();
                    elementExists = driver.FindElements(pfpEmailLabel).Any();

                    if (elementExists)
                        break;
                }
            }

            wait.Until(ExpectedConditions.ElementIsVisible(pfpEmailLabel));
            driver.FindElement(pfpEmailLabel).Click();
            Thread.Sleep(TimeSpan.FromSeconds(5));

            var emailInfoFrame = By.Id("messageframe");

            wait.Until(ExpectedConditions.ElementIsVisible(emailInfoFrame));

            var winHandleBefore = driver.CurrentWindowHandle;

            driver.SwitchTo().Frame(driver.FindElement(emailInfoFrame));

            var address = DriverExtensions.GetRedirectPath();

            var activateRegistrationLink = By.XPath($"//a[contains(@href,'{address}')]");

           var redirectUrl =  driver.FindElement(activateRegistrationLink).GetAttribute("href");
            driver.SwitchTo().Window(winHandleBefore);

            return redirectUrl;
        }

        private bool NavigateToInbox(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            driver.Navigate().GoToUrl("https://www.maildrop.cc/");

            var emailInput = driver.FindElementById("nav-inbox");

            emailInput.Clear();
            emailInput.SendKeys(mailboxPrefix);
            emailInput.SendKeys(Keys.Enter);

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(20),
                PollingInterval = TimeSpan.FromMilliseconds(300)
            };

            try
            {
                wait.IgnoreExceptionTypes(typeof (NoSuchElementException));

                var mainAddressLabel = By.Id("mainaddr");

                wait.Until(ExpectedConditions.ElementIsVisible(mainAddressLabel));

                return true;
            }
            catch (TimeoutException)
            {
                return false;
            }
        }
    }
}