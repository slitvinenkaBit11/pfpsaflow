﻿using OpenQA.Selenium.Chrome;

namespace PFPAutomation.Services
{
    public interface IMailService
    {
        string CreateMailBox(string mailboxPrefix, ChromeDriver driver);
        string GetRegistrationLink(string mailboxPrefix, ChromeDriver driver);
    }
}