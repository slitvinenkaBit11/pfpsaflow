﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using PFPAutomation.Helpers;

namespace PFPAutomation.Services
{
    public class MailinatorService : IMailService
    {
        public string CreateMailBox(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            var available = NavigateToInbox(mailboxPrefix, driver);
            var mailbox = "";

            if (!available) return mailbox;

            mailbox = $"{mailboxPrefix}@mailinator.com";

            return mailbox;
        }

        public string GetRegistrationLink(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            NavigateToInbox(mailboxPrefix, driver);

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(20),
                PollingInterval = TimeSpan.FromMilliseconds(300)
            };

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            var pfpEmailLabel = By.XPath("//div[contains(@class,'innermail ng-binding')][contains(text(),'Personal Finance Portal')]");
            var elementExists = driver.FindElements(pfpEmailLabel).Any();

            if (!elementExists)
            {
                for (var i = 0; i < 20; i++)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    driver.Navigate().Refresh();
                    elementExists = driver.FindElements(pfpEmailLabel).Any();

                    if (elementExists) break;
                }
            }

            wait.Until(ExpectedConditions.ElementIsVisible(pfpEmailLabel));
            driver.FindElement(pfpEmailLabel).Click();

            var emailInfo = By.Id("publicShowMailCtrl");

            wait.Until(ExpectedConditions.ElementIsVisible(emailInfo));

            var winHandleBefore = driver.CurrentWindowHandle;
            var emailContent = By.Id("publicshowmaildivcontent");

            wait.Until(ExpectedConditions.ElementIsVisible(emailContent));
            driver.SwitchTo().Frame(driver.FindElement(emailContent));

            var activateRegistrationLink = By.XPath($"//a[contains(@href,'{DriverExtensions.GetRedirectPath()}')]");
            var redirectUrl = driver.FindElement(activateRegistrationLink).GetAttribute("href");

            driver.SwitchTo().Window(winHandleBefore);

            return redirectUrl;
        }

        private bool NavigateToInbox(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            driver.Navigate().GoToUrl("https://www.mailinator.com/");

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(20),
                PollingInterval = TimeSpan.FromMilliseconds(300)
            };
            var emailInputSelection = By.Id("inboxfield");

            try
            {
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                wait.Until(ExpectedConditions.ElementIsVisible(emailInputSelection));

                var emailInput = driver.FindElement(emailInputSelection);

                emailInput.SendKeys(mailboxPrefix);
                emailInput.SendKeys(Keys.Enter);

                var inboxInput = By.XPath($"//input[contains(@value,'{mailboxPrefix}')]");

                wait.Until(ExpectedConditions.ElementIsVisible(inboxInput));

                return true;
            }
            catch (TimeoutException)
            {
                return true;
            }

        }
    }
}