﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using PFPAutomation.Helpers;

namespace PFPAutomation.Services
{
    public class TenMinuteEmailService : IMailService
    {
        public string CreateMailBox(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            var available = NavigateToInbox(mailboxPrefix, driver);
            var mailbox = "";

            if (!available) return mailbox;

            var mailAddress = By.Id("mailAddress");

            mailbox = driver.FindElement(mailAddress).GetAttribute("value");

            return mailbox;
        }

        public string GetRegistrationLink(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            NavigateToInbox(mailboxPrefix, driver);

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(20),
                PollingInterval = TimeSpan.FromMilliseconds(300)
            };

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            var pfpEmailLabel = By.XPath("//span[contains(text(),'Personal Finance Portal - Registration')]");
            var elementExists = driver.FindElements(pfpEmailLabel).Any();

            if (!elementExists)
            {
                for (var i = 0; i < 20; i++)
                {
                    driver.Navigate().Refresh();
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    elementExists = driver.FindElements(pfpEmailLabel).Any();

                    if (elementExists) break;
                }
            }

            wait.Until(ExpectedConditions.ElementIsVisible(pfpEmailLabel));
            driver.FindElement(pfpEmailLabel).Click();

            var activateRegistrationLink = By.XPath($"//a[contains(@href, '{DriverExtensions.GetRedirectPath()}')]");
            var redirectUrl = driver.FindElement(activateRegistrationLink).GetAttribute("href");

            return redirectUrl;
        }

        private bool NavigateToInbox(string mailboxPrefix, ChromeDriver driver)
        {
            if (mailboxPrefix == null)
                throw new ArgumentNullException(nameof(mailboxPrefix));

            driver.Navigate().GoToUrl("https://10minutemail.com");

            var resetSessionLifeButton = By.Id("resetSessionLifeButton");
            var mailAddress = By.Id("mailAddress");
            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(20),
                PollingInterval = TimeSpan.FromMilliseconds(300)
            };

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            try
            {
                wait.Until(ExpectedConditions.ElementIsVisible(resetSessionLifeButton));
                driver.FindElement(resetSessionLifeButton).Click();
                wait.Until(ExpectedConditions.ElementIsVisible(mailAddress));

                return true;
            }
            catch (TimeoutException)
            {
                return false;
            }
        }
    }
}