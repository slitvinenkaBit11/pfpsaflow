﻿using System;
using System.Configuration;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using PFPAutomation.Models;

namespace PFPAutomation.Helpers
{
    public class ClientWizardHelper
    {
        public void CreateSingleClient(Client client, ChromeDriver driver)
        {
            var winHandleBefore = driver.CurrentWindowHandle;
            var addClientLink = driver.FindElements(By.CssSelector(".nav-quicklinks >li")).FirstOrDefault(x => x.Text == "Add Client");

            if (addClientLink == null) return;

            addClientLink.Click();

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                Timeout = TimeSpan.FromSeconds(20),
                PollingInterval = TimeSpan.FromMilliseconds(300)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            var clientPopup = By.XPath("//iframe[@src='/nio/clientactions/addclient']");

            wait.WaitElenentIsVisible("//iframe[@src='/nio/clientactions/addclient']");
            driver.SwitchTo().Frame(driver.FindElement(clientPopup));

            var clientTypeDropdown = driver.FindElementById("ClientType");
            var selectElement = new SelectElement(clientTypeDropdown);

            selectElement.SelectByText(client.Type.ToString());
            driver.FindElementById("Life1FirstName").SendKeys(client.FirstName);
            driver.FindElementById("Life1LastName").SendKeys(client.LastName);
            driver.FindElementById("id_BasicDetailsStep_FirstLife_DateOfBirth").SendKeys($"{client.Dob:dd/MM/yyyy}");

            var genderDropdowm = driver.FindElementById("id_BasicDetailsStep_FirstLife_Gender");

            selectElement = new SelectElement(genderDropdowm);
            selectElement.SelectByText(client.Gender.ToString());

            var localTesting = Convert.ToBoolean(ConfigurationManager.AppSettings["LocalTesting"]);

            if (localTesting)
            {
                driver.FindElementById("Life1ExternalReference").SendKeys(Convert.ToString(client.CustomerRef));
            }

            var niNumber = By.Id("id_BasicDetailsStep_FirstLife_NINumber");

            driver.FindElement(niNumber).SendKeys(client.NiNumber);

            try
            {
                var customerReference = By.Id("Life1ExternalReference");
                var customerRefElement = driver.FindElement(customerReference);
                customerRefElement?.SendKeys(client.CustomerRef.ToString());
            }
            catch (Exception)
            {
                Console.WriteLine("Customer reference was not found");
            }

            var nextButton = By.XPath("//a[contains(@class,'next button button-enabled')]");
            var completeButton = By.XPath("//a[contains(@class,'complete button button-enabled')]");

            driver.FindElement(nextButton).Click(); //Address

            var spinner = By.XPath("//div[contains(@class,'blockUI blockOverlay')]");
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(spinner));
            driver.FindElement(nextButton).Click(); //Contact

            var emailField = By.Id("FrstLifeEml");

            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(spinner));
            wait.Until(ExpectedConditions.ElementIsVisible(emailField));
            driver.FindElement(emailField).SendKeys(client.Email);

            var serviceStatusSelector = By.Id("OpportunityStep.FirstServiceStatusId");

            driver.FindElement(nextButton).Click(); //Opportunity
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(spinner));
            wait.Until(ExpectedConditions.ElementIsVisible(serviceStatusSelector));

            var serviceStatusElement = driver.GetElementByXPath("//select[@id='OpportunityStep.FirstServiceStatusId']/optgroup[@label='Tenant Level']/option[contains(text(),'Medium')]");
            var lowServiceStatusElement = driver.GetElementByXPath("//select[@id='OpportunityStep.FirstServiceStatusId']/optgroup[@label='Low']/option[contains(text(),'Low')]");
            (serviceStatusElement ?? lowServiceStatusElement)?.Click();

            driver.FindElement(completeButton).Click();
            driver.SwitchTo().Window(winHandleBefore);
        }


    }
}