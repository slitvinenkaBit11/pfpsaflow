﻿using System;
using System.Configuration;
using OpenQA.Selenium.Chrome;
using PFPAutomation.Models;

namespace PFPAutomation.Helpers
{
    public class IntelligentOfficeHelper
    {
        private static readonly LoginHelper loginHelper = new LoginHelper();
        private static readonly ClientWizardHelper wizardHelper = new ClientWizardHelper();

        public void CreateClient(ChromeDriver driver, Client client)
        {
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            driver.Navigate().GoToUrl( ConfigurationManager.AppSettings["IoUrl"]);
            loginHelper.LoginToIntelligentOffice(driver);
            wizardHelper.CreateSingleClient(client, driver);
        }
    }
}