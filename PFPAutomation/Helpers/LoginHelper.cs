﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PFPAutomation.Models;

namespace PFPAutomation.Helpers
{
    public class LoginHelper
    {
        public void LoginToIntelligentOffice(ChromeDriver driver)
        {
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));

            var login =  ConfigurationManager.AppSettings["Adviser.Username"];
            var pswd = ConfigurationManager.AppSettings["Adviser.Password"];
            var userNameField = driver.FindElementById("user");
            var userPasswordField = driver.FindElementById("password");
            var loginButton = driver.FindElementByClassName("login_button");

            userNameField.SendKeys(login);
            userPasswordField.SendKeys(pswd);
            loginButton.Click();
        }

        public void LoginToPfp(Client client,ChromeDriver driver)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));

            driver.FindElementById("username").SendKeys(client.Email);
            driver.FindElementById("password").SendKeys(client.PfpPassword);
            driver.FindElement(By.XPath("//button[contains(@class,'btn btn-primary')][contains(text(),'Login')]")).Click();
        }
    }
}