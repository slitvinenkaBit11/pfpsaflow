﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace PFPAutomation.Helpers
{
    public class PfpOverviewHelper
    {
        public void RunSimpleAdviceFlow(ChromeDriver driver)
        {
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                PollingInterval = TimeSpan.FromMilliseconds(300),
                Timeout = TimeSpan.FromSeconds(30)
            };
            var overviewLink = By.XPath("//a[contains(@href,'/overview')][contains(text(),'Overview')]");

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            wait.Until(ExpectedConditions.ElementIsVisible(overviewLink));
            driver.FindElement(overviewLink).Click();

            var planningAndAdviceLink = By.XPath("//span[contains(text(),'Planning & Advice')]");

            wait.Until(ExpectedConditions.ElementIsVisible(planningAndAdviceLink));
            driver.FindElement(planningAndAdviceLink).Click();

            var planningAndAdviceTitle = By.XPath("//span[contains(text(),'Looking for a quick, simple investment? Start here')]");

            wait.Until(ExpectedConditions.ElementIsVisible(planningAndAdviceTitle));
            driver.FindElementById("isDisclosureRead").Click();
            driver.FindElementById("hasNoDebts").Click();

            var startPlanningButton = By.Id("startPlanningBtn");

            wait.Until(ExpectedConditions.ElementToBeClickable(startPlanningButton));
            driver.FindElement(startPlanningButton).Click();

            var howServiceWorksTitle = By.XPath("//h1[contains(text(),'How the service works')]");

            wait.Until(ExpectedConditions.ElementIsVisible(howServiceWorksTitle));

            var nextButton = By.XPath("//a[contains(@class,'button button-next')]");

            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var yourInvestmentTitle = By.XPath("//h1[contains(text(),'Your investment')]");

            wait.Until(ExpectedConditions.ElementIsVisible(yourInvestmentTitle));
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var attitudeToInvestmentRiskTitle = By.XPath("//h1[contains(text(),'About your attitude to investment risk')]");

            wait.Until(ExpectedConditions.ElementIsVisible(attitudeToInvestmentRiskTitle));
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var riskTolleranceQuestionsTitle = By.XPath("//h1[contains(text(),'Risk tolerance questions')]");

            wait.Until(ExpectedConditions.ElementIsVisible(riskTolleranceQuestionsTitle));

            var firstQuestionLabel = By.XPath("//b[contains(text(),'Question 1 of 10')]");
            var secondQuestionLabel = By.XPath("//b[contains(text(),'Question 2 of 10')]");
            var thirdQuestionLabel = By.XPath("//b[contains(text(),'Question 3 of 10')]");
            var fourthQuestionLabel = By.XPath("//b[contains(text(),'Question 4 of 10')]");
            var fifthQuestionLabel = By.XPath("//b[contains(text(),'Question 5 of 10')]");
            var sixthQuestionLabel = By.XPath("//b[contains(text(),'Question 6 of 10')]");
            var seventhQuestionLabel = By.XPath("//b[contains(text(),'Question 7 of 10')]");
            var eighthQuestionLabel = By.XPath("//b[contains(text(),'Question 8 of 10')]");
            var ninthQuestionLabel = By.XPath("//b[contains(text(),'Question 9 of 10')]");
            var tenthQuestionLabel = By.XPath("//b[contains(text(),'Question 10 of 10')]");

            wait.Until(ExpectedConditions.ElementIsVisible(firstQuestionLabel));
            driver.FindElement(By.Id("question_0_answer_6")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(secondQuestionLabel));
            driver.FindElement(By.Id("question_1_answer_6")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(thirdQuestionLabel));
            driver.FindElement(By.Id("question_2_answer_6")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(fourthQuestionLabel));
            driver.FindElement(By.Id("question_3_answer_6")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(fifthQuestionLabel));
            driver.FindElement(By.Id("question_4_answer_6")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(sixthQuestionLabel));
            driver.FindElement(By.Id("question_5_answer_6")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(seventhQuestionLabel));
            driver.FindElement(By.Id("question_6_answer_6")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(eighthQuestionLabel));
            driver.FindElement(By.Id("question_7_answer_6")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(ninthQuestionLabel));
            driver.FindElement(By.Id("question_8_answer_6")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(tenthQuestionLabel));
            driver.FindElement(By.Id("question_9_answer_6")).Click();


            var yourRiskToleranceProfileTitle = By.XPath("//h1[contains(text(),'Your risk tolerance profile')]");

            wait.Until(ExpectedConditions.ElementIsVisible(yourRiskToleranceProfileTitle));
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var yourInvestmentProjectionTitle = By.XPath("//h1[contains(text(),'Your investment projection')]");

            wait.Until(ExpectedConditions.ElementIsVisible(yourInvestmentProjectionTitle));
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var aboutYourRiskCapacityTitle = By.XPath("//h1[contains(text(),'About your risk capacity')]");

            wait.Until(ExpectedConditions.ElementIsVisible(aboutYourRiskCapacityTitle));
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var firstRiskCapacityQuestion = By.XPath("//h2[contains(text(),'Question 1 of 3')]");
            var secondRiskCapacityQuestion = By.XPath("//h2[contains(text(),'Question 2 of 3')]");
            var thirdRiskCapacityQuestion = By.XPath("//h2[contains(text(),'Question 3 of 3')]");

            wait.Until(ExpectedConditions.ElementIsVisible(firstRiskCapacityQuestion));
            driver.FindElement(By.Id("question_0_answer_3")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(secondRiskCapacityQuestion));
            driver.FindElement(By.Id("question_1_answer_2")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(thirdRiskCapacityQuestion));
            driver.FindElement(By.Id("question_2_answer_3")).Click();

            var yourProposedRiskCategoryTitle = By.XPath("//h1[contains(text(),'Your proposed risk category')]");

            wait.Until(ExpectedConditions.ElementIsVisible(yourProposedRiskCategoryTitle));
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var confirmInvestmentChoicesTitle = By.XPath("//h1[contains(text(),'Please confirm your investment choices')]");

            wait.Until(ExpectedConditions.ElementIsVisible(confirmInvestmentChoicesTitle));
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var ourRecommendationsTitle = By.XPath("//h1[contains(text(),'Our recommendation for you')]");

            wait.Until(ExpectedConditions.ElementIsVisible(ourRecommendationsTitle));
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var confirmYourDetailsTitle = By.XPath("//h1[contains(text(),'Confirm your details')]");

            wait.Until(ExpectedConditions.ElementIsVisible(confirmYourDetailsTitle));
            driver.FindElementById("pers-isUKResident").Click();
            driver.FindElementById("pers-isNotUSCitizen").Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var declarationTitle = By.XPath("//h1[contains(text(),'Declaration')]");

            wait.Until(ExpectedConditions.ElementIsVisible(declarationTitle));
            driver.FindElementById("tenantTandCConfirmed").Click();
            driver.FindElementById("platformTandCConfirmed").Click();
            driver.FindElementById("platformCanDeductCashAndDisinvestConfirmed").Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var proceedToPaymentTitle = By.XPath("//h1[contains(text(),'Proceed to payment')]");

            wait.Until(ExpectedConditions.ElementIsVisible(proceedToPaymentTitle));
            wait.Until(ExpectedConditions.ElementToBeClickable(nextButton));
            driver.FindElement(nextButton).Click();

            var securePaymentPageLabel = By.XPath("//h1[contains(text(),'Secure Payment Page')]");

            wait.Until(ExpectedConditions.ElementIsVisible(securePaymentPageLabel));
        }
    }
}
