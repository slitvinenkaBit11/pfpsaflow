﻿using System;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using PFPAutomation.Models;

namespace PFPAutomation.Helpers
{
    public class PfpMembershipHelper
    {
        public void ConfirmExistingInfo(Client client, ChromeDriver driver, string redirectUrl)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                PollingInterval = TimeSpan.FromMilliseconds(300),
                Timeout = TimeSpan.FromSeconds(20)
            };

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

            driver.Navigate().GoToUrl(redirectUrl);

            var confirmLabel = By.XPath("//span[contains(text(),'Confirm Existing Security Information')]");

            wait.Until(ExpectedConditions.ElementIsVisible(confirmLabel));

            driver.FindElementById("IdentityData1Answer").SendKeys($"{client.Dob:dd/MM/yyyy}");
            driver.FindElementById("IdentityData2Answer").SendKeys(client.NiNumber);
            driver.FindElementById("NewPassword").SendKeys(client.PfpPassword);
            driver.FindElementById("ConfirmedPassword").SendKeys(client.PfpPassword);
            driver.FindElementById("SecurityQuestion1Answer").SendKeys("Goga");

            var clientTypeDropdown = driver.FindElementById("SecurityQuestion2");
            var selectElement = new SelectElement(clientTypeDropdown);

            selectElement.SelectByText("What was the last name of your childhood best friend");
            driver.FindElementById("SecurityQuestion2Answer").SendKeys("Goga");
            driver.FindElementByName("TermsAndConditionsAccepted").Click();
            driver.FindElementByName("PrivacyAndCookiesPolicyAccepted").Click();
            driver.FindElement(By.XPath("//button[contains(@class,'btn btn-primary btn-block')]")).Click();

            var activationCompleteLabel = By.XPath("//div[contains(text(),'Activation process complete')]");

            wait.Until(ExpectedConditions.ElementIsVisible(activationCompleteLabel));

            var loginLink = By.XPath($"//a[contains(@href,'{DriverExtensions.GetLoginUrl()}')]");

            driver.FindElement(loginLink).Click();
        }
    }
}