﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using PFPAutomation.Models;

namespace PFPAutomation.Helpers
{
    public class PfpProfileHelper
    {
        public void SetUpAddressDetails(Client client, ChromeDriver driver)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                PollingInterval = TimeSpan.FromMilliseconds(300),
                Timeout = TimeSpan.FromSeconds(20)
            };
            var profileLink = By.XPath("//a[contains(@href,'/profile')][contains(text(),'Profile')]");

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            wait.Until(ExpectedConditions.ElementIsVisible(profileLink));
            driver.FindElement(profileLink).Click();

            var editAddressDetailsLink = By.XPath("//a[contains(@href,'/profile/editaddress')]");

            wait.Until(ExpectedConditions.ElementIsVisible(editAddressDetailsLink));
            driver.FindElement(editAddressDetailsLink).Click();

            var addAddressButton= By.XPath("//a[contains(@href,'/profile/addaddress')][contains(text(),'Add address')]");

            wait.Until(ExpectedConditions.ElementToBeClickable(addAddressButton));
            driver.FindElement(addAddressButton).Click();

            var prefferedContactAddress = By.XPath("//label[contains(text(),'Preferred Contact Address')]");

            wait.Until(ExpectedConditions.ElementToBeClickable(prefferedContactAddress));
            driver.FindElement(prefferedContactAddress).Click();
            driver.FindElementById("pers-addr-line1").SendKeys(client.AddressLine1);
            driver.FindElementById("pers-addr-locality").SendKeys(client.Town);
            driver.FindElementById("pers-addr-line2").SendKeys(client.AddressLine2);
            driver.FindElementById("pers-addr-line3").SendKeys(client.AddressLine3);
            driver.FindElementById("pers-addr-line4").SendKeys(client.AddressLine4);
            driver.FindElement(By.XPath($"//select[@id='pers-addr-county']/option[contains(text(),'{client.County}')]")).Click();
            driver.FindElementById("pers-addr-postalcode").SendKeys(client.Postcode);
            driver.FindElement(By.XPath($"//select[@id='pers-addr-country']/option[contains(text(),'{client.Country}')]")).Click();
            driver.FindElementById("pers-addr-resident-from").SendKeys($"{client.ResidentFrom:dd/MM/yyyy}");
            driver.FindElement(By.XPath("//input[contains(@class,'button-right button-half-width')][contains(@value,'Save and back')]")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//h2[contains(text(),'Profile Information')]")));
        }

        public void SetUpContactDetails(Client client,ChromeDriver driver)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                PollingInterval = TimeSpan.FromMilliseconds(300),
                Timeout = TimeSpan.FromSeconds(20)
            };
            var profileLink = By.XPath("//a[contains(@href,'/profile')][contains(text(),'Profile')]");

            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            wait.Until(ExpectedConditions.ElementIsVisible(profileLink));
            driver.FindElement(profileLink).Click();

            var editContactDetailsLink = By.XPath("//a[contains(@href,'/profile/editcontact')]");

            wait.Until(ExpectedConditions.ElementIsVisible(editContactDetailsLink));
            driver.FindElement(editContactDetailsLink).Click();

            var addContactButton = By.XPath("//a[contains(@href,'/profile/addcontacttype')][contains(text(),'Add contact detail')]");

            wait.Until(ExpectedConditions.ElementToBeClickable(addContactButton));
            driver.FindElement(addContactButton).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("ContactType_ContactType")));
            driver.FindElement(By.XPath("//select[@id='ContactType_ContactType']/option[contains(text(),'Telephone')]")).Click();
            driver.FindElementById("ContactType_Value").SendKeys(client.Telephone);
            driver.FindElementById("ContactType_Value").SendKeys(Keys.Enter);

            var saveButton =By.XPath("//input[contains(@class,'button-right button-half-width')][contains(@value,'Save and back')]");

            wait.Until(ExpectedConditions.ElementToBeClickable(saveButton));
            driver.FindElement(saveButton).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//h2[contains(text(),'Profile Information')]")));
        }
    }
}