using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace PFPAutomation.Helpers
{
    public static class DriverExtensions
    {
        public static IWebElement GetElementByXPath(this ChromeDriver driver, string xPath)
        {
            try
            {
                return driver.FindElement(By.XPath(xPath));
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static void WaitElenentIsVisible(this DefaultWait<IWebDriver> wait, string xPath)
        {
            while (true)
            {
                try
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xPath)));
                    return;
                }
                catch (Exception)
                {
                }
            }
        }

        public static string GetRedirectPath()
        {
            var address = ConfigurationManager.AppSettings["PfpUrl"];
            if (!address.EndsWith("/"))
                address += "/";
            address += "Account/Verify";

            return address;
        }

        public static string GetLoginUrl()
        {
            var address = ConfigurationManager.AppSettings["PfpUrl"];
            if (!address.EndsWith("/"))
                address += "/";
            address += "account/verifiedtoken";
            return address;
        }
    }
}