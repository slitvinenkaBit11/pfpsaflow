﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using PFPAutomation.Models;

namespace PFPAutomation.Helpers
{
    public class PfpHelper
    {
        private static readonly LoginHelper loginHelper = new LoginHelper();
        private static readonly PfpProfileHelper pfpProfileHelper = new PfpProfileHelper();
        private static readonly PfpOverviewHelper pfpOverviewHelper = new PfpOverviewHelper();

        public void Register(ChromeDriver driver, string email)
        {
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));
            if (email == null)
                throw new ArgumentNullException(nameof(email));

            var wait = new DefaultWait<IWebDriver>(driver)
            {
                PollingInterval = TimeSpan.FromMilliseconds(300),
                Timeout = TimeSpan.FromSeconds(60)
            };

            RedirectToPfp(driver, wait);

            var emailField = By.Id("Email");

            wait.Until(ExpectedConditions.ElementIsVisible(emailField));
            driver.FindElement(emailField).SendKeys(email);

            var captchaElement = By.XPath("//div[@class='recaptcha-checkbox-checkmark']");

            var captchaFrame = By.XPath("//iframe[contains(@src, 'https://www.google.com/recaptcha')]");
            var winHandleBefore = driver.CurrentWindowHandle;

            wait.Until(ExpectedConditions.ElementIsVisible(captchaFrame));
            driver.SwitchTo().Frame(driver.FindElement(captchaFrame));
            wait.Until(ExpectedConditions.ElementIsVisible(captchaElement));
            driver.FindElement(captchaElement).Click();

            Console.WriteLine("Please Enter the Captcha in PFP application and press ENTER to go further...");
            Console.ReadLine();
            Console.WriteLine("Going further...");
            driver.SwitchTo().Window(winHandleBefore);
            driver.SwitchTo().Frame(driver.FindElement(captchaFrame));
            wait.Until(ExpectedConditions.ElementIsVisible(captchaElement));
            driver.SwitchTo().Window(winHandleBefore);

            var submitButton = By.XPath("//input[@value='Start Registration']");

            wait.Until(ExpectedConditions.ElementToBeClickable(submitButton));
            driver.FindElement(submitButton).Click();

            var registrationLabel = By.XPath("//h1[contains(text(),'Thanks for Registering!')]");

            wait.Until(ExpectedConditions.ElementIsVisible(registrationLabel));
        }

        private static void RedirectToPfp(ChromeDriver driver, DefaultWait<IWebDriver> wait)
        {
            for (var i = 0; i < 10; i++)
            {
                driver.Navigate().GoToUrl(new Uri(ConfigurationManager.AppSettings["PfpUrl"]));

                var registerButton = By.XPath("//a[@href='/account/register']");

                try
                {
                    wait.Until(ExpectedConditions.ElementIsVisible(registerButton));
                    driver.FindElement(registerButton).Click();
                    return;
                }
                catch (Exception)
                {
                    Console.WriteLine("PFP redirect failed. Attent count: " + (i + 1));
                }
            }
        }

        public void RunThroughTheFlow(Client client, ChromeDriver driver)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));
            if (driver == null)
                throw new ArgumentNullException(nameof(driver));

            loginHelper.LoginToPfp(client, driver);

            if (bool.Parse(ConfigurationManager.AppSettings["CreateClientOnly"]))
                return;

            pfpProfileHelper.SetUpAddressDetails(client,driver);
            pfpProfileHelper.SetUpContactDetails(client, driver);
            pfpOverviewHelper.RunSimpleAdviceFlow(driver);
        }
    }
}