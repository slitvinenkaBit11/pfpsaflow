﻿using System;
using System.IO;
using OpenQA.Selenium.Chrome;
using PFPAutomation.Factories;
using PFPAutomation.Helpers;
using PFPAutomation.Services;

namespace PFPAutomation
{
    class TestRunner
    {

        private const string ClientFileName = "client-details.txt";

        private static readonly ClientFactory clientFactory = new ClientFactory();
        private static readonly IntelligentOfficeHelper intelligentOfficeHelper = new IntelligentOfficeHelper();
        private static readonly PfpHelper pfpHelper = new PfpHelper();
        private static readonly PfpMembershipHelper pfpMembershipHelper = new PfpMembershipHelper();
        private static readonly MailinatorService mailinatorService = new MailinatorService();
        private static readonly MaildropService maildropService = new MaildropService();
        private static readonly TenMinuteEmailService tenMinuteEmailService = new TenMinuteEmailService();
        private static readonly ThrowAwayMailService throwAwayMailService = new ThrowAwayMailService();
        private static readonly CompositeMailService compositeMailService = new CompositeMailService(new IMailService[]
        {
            tenMinuteEmailService, throwAwayMailService, maildropService,  mailinatorService
        });

        static void Main()
        {
            var driver = new ChromeDriver();

            driver.Manage().Window.Maximize();

            var mailPrefix = "io-test-" + DateTime.Now.Ticks;
            var mailAddress = compositeMailService.GetMailAddress(mailPrefix, driver);
            var client = clientFactory.Create(mailAddress);

            if (File.Exists(ClientFileName))
                File.Delete(ClientFileName);

            File.WriteAllText(ClientFileName, $"Login: {client.Email}, Password: {client.PfpPassword}");


            // Go to the home page, login and create client
            intelligentOfficeHelper.CreateClient(driver, client);
            //Start registration in PFP
            pfpHelper.Register(driver, client.Email);

            var link = compositeMailService.GetRegistrationLink(mailPrefix, driver);

            pfpMembershipHelper.ConfirmExistingInfo(client, driver, link);
            pfpHelper.RunThroughTheFlow(client, driver);
        }
    }
}